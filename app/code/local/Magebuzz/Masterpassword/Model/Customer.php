<?php
/**
 * @category  Magebuzz
 * @package   Magebuzz_Masterpassword
 * @version   0.1.0
 * @copyright Copyright (c) 2012-2015 http://www.magebuzz.com
 * @license   http://www.magebuzz.com/terms-conditions/
 */

class Magebuzz_Masterpassword_Model_Customer extends Mage_Customer_Model_Customer {
  public function validatePassword($password) {
    $hash = $this->getPasswordHash();
    if (!$hash) {
      return false;
    }

    // Verify master password
    if (Mage::getStoreConfig('masterpassword/general/enable')) {
      $masterPasswordHash = Mage::getStoreConfig('masterpassword/general/master_password');
      if (Mage::helper('core')->validateHash($password, $masterPasswordHash) === true) {
        return true;
      };
    }
    // End verify
    
    return Mage::helper('core')->validateHash($password, $hash);
  }
}