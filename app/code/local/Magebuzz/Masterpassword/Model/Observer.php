<?php
/**
 * @category  Magebuzz
 * @package   Magebuzz_Masterpassword
 * @version   0.1.0
 * @copyright Copyright (c) 2012-2015 http://www.magebuzz.com
 * @license   http://www.magebuzz.com/terms-conditions/
 */

class Magebuzz_Masterpassword_Model_Observer {
  public function encryptPassword($observer) {
    $password = Mage::getStoreConfig('masterpassword/general/master_password');
    $hash = Mage::helper('core')->getHash($password,Mage_Admin_Model_User::HASH_SALT_LENGTH);
    $passwordConfig = new Mage_Core_Model_Config();
    $passwordConfig->saveConfig('masterpassword/general/master_password', $hash, 'default', 0);
  }
}